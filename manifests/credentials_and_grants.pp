define database_postgresql::credentials_and_grants (
  $username,
  $password_hash,
  $database,
  $pg_hba_address      = undef,
  $pg_hba_auth_method  = 'md5',
  $pg_hba_type         = 'host',
  $schema              = 'public',
  $create_user         = true,
  $create_db_grants    = true,
  $grant_all_tables    = true,
  $grant_all_sequences = true,) {

  if ($create_user) {
	  postgresql::server::role { $username:
	    password_hash => $password_hash,
	  }
  }

  if ($create_db_grants) {
	  postgresql::server::database_grant { "${username}_${database}":
	    privilege => 'ALL',
	    db        => $database,
	    role      => $username,
	  }
  }

  if ($grant_all_tables) {
    postgresql::server::grant { "${username}_${database}_all_tables_in_schema":
      privilege   => 'ALL',
      db          => $database,
      object_type => 'ALL TABLES IN SCHEMA',
      object_name => $schema,
      role        => $username,
    }
  }

  if ($grant_all_sequences) {
    postgresql::server::grant { "${username}_${database}_all_sequences_in_schema":
      privilege   => 'ALL',
      db          => $database,
      object_type => 'ALL SEQUENCES IN SCHEMA',
      object_name => $database,
      role        => $username,
    }
  }

  if ($pg_hba_address) {
	  postgresql::server::pg_hba_rule { "allow ${pg_hba_address} server to access ${database} database_${username}":
	    description => "Open up PostgreSQL for access from ${pg_hba_address} to ${database}",
	    type        => $pg_hba_type,
	    database    => $database,
	    user        => $username,
	    address     => $pg_hba_address,
	    auth_method => $pg_hba_auth_method,
	  }
  }
}