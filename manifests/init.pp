# Class: database_postgresql
#
# This module manages database_postgresql
#
# Parameters: none
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class database_postgresql (
  $ip_mask_deny_postgres_user = $database_postgresql::params::ip_mask_deny_postgres_user,
  $listen_addresses           = $database_postgresql::params::listen_addresses,
  $postgresql_version         = $database_postgresql::params::postgresql_version
) inherits database_postgresql::params {

  class { 'database_postgresql::setup':
    postgresql_version          => $postgresql_version,
    ip_mask_deny_postgres_user  => $ip_mask_deny_postgres_user,
    listen_addresses            => $listen_addresses
  }
}