class database_postgresql::server (
  $ip_mask_deny_postgres_user,
  $listen_addresses
) inherits database_postgresql::params {

	class { 'postgresql::server':
		ip_mask_deny_postgres_user => '0.0.0.0/32',
		listen_addresses           => '*',
	}
}