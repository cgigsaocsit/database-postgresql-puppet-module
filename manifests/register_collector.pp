define database_postgresql::register_collector (
  $dbms                   = 'postgresql',
  $user_registration_file = undef,
  $register_tag           = undef,
) {

  database_credential_collector::register_collector { $dbms:
    user_registration_file  => $user_registration_file,
    register_tag            => $register_tag,
  }
}