class database_postgresql::register (
  $database       = $title,
  $dbms           = 'postgresql',
  $user,
  $hostname       = undef,
  $register_tag   = undef,
) {

  database_credential_collector::register { $database:
    dbms          => $dbms,
    user          => $user,
    hostname      => $hostname,
    register_tag  => $register_tag,
  }
}