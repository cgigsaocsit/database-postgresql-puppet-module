require 'rubygems'
require 'yaml'
require 'digest/sha1'
require 'securerandom'
require 'fileutils'
require	'facter'

module Puppet::Parser::Functions
	newfunction(:store_and_generate_postgresql_password_hash, :type => :rvalue) do |args|
		STORE_DIR = '/var/lib/puppet/store_and_generate_postgresql'
		server_fqdn = lookupvar('fqdn')
		db_name = args[0]
		file_name = "#{server_fqdn}_#{db_name}.yml"
		full_path = File.join(STORE_DIR, file_name)

		password_exists = false

		if File.exist?(full_path)
			password_hash = YAML.load_file(full_path)

			password_exists = true if password_hash and password_hash['password'] and password_hash['hash'] and password_hash['user']
		end

		if password_exists
			# Update the mtime
			FileUtils.touch full_path
		else
			user	= "auto_#{SecureRandom.hex(5)}"
			pass 	= SecureRandom.hex

			password_hash 	= {
				'hash'		=> function_postgresql_password( [ user, pass ] ),
				'password'	=> pass,
				'user'		=> user
			}

			password_hash_yaml = password_hash.to_yaml

			File.open(full_path,"w") do |f|
				f.write(password_hash_yaml)
			end			
		end

		return password_hash
	end
end
