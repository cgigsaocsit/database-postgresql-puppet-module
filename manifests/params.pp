class database_postgresql::params {
  # Server
  $listen_addresses           = '*'
  $ip_mask_deny_postgres_user = '0.0.0.0/32'

  $postgresql_version         = '9.2'

  $dbms_name                  = 'postgresql'
}