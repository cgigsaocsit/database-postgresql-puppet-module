class database_postgresql::setup (
  $ip_mask_deny_postgres_user,
  $listen_addresses,
  $postgresql_version
) inherits database_postgresql::params {

  class { 'postgresql::globals':
		manage_package_repo => true,
		version             => $postgresql_version,
  }->
  class { 'database_postgresql::server':
    ip_mask_deny_postgres_user  => $ip_mask_deny_postgres_user,
    listen_addresses            => $listen_addresses
  }
}